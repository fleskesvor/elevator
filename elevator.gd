extends KinematicBody2D

var floors = []
var going_down = true
var destination = null

func _input(event):
	if event.is_pressed():
		for node in floors:
			if event.is_action(node.get_name()):
				if destination == node:
					return
				destination = node
		if destination != null:
			going_down = destination.get_pos().y > get_pos().y
			set_process(true)

func _process(delta):
	if going_down and get_pos().y > destination.get_pos().y \
		or not going_down and get_pos().y < destination.get_pos().y:
		set_process(false)
	else:
		move(Vector2(0, 10 if going_down else -10))

func _ready():
	for node in get_parent().get_children():
		if node.is_type("Label"):
			floors.append(node)
	set_process_input(true)
